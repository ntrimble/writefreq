#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <math.h>

#define fs 48000
#define pi 3.1415926535

int writeNewFrequency(int*);

double newFreq280, newFreqNorm;
uint32_t newDiscreteFrequency;

int main(int argc, char *argv[]) 
{
  if (argc != 2) {
    printf("\nUsage: %s frequency\n\n", argv[0]);
  } else {
    printf("Input\t\t%s Hz\n",argv[1]);

    /* Convert input string to integer */
    int inputFreq = atoi(argv[1]);

    writeNewFrequency(&inputFreq);

    printf("Norm. Freq.\t%f Rads/sec\n",newFreqNorm);
    printf("In 28.0\t\t%f\n",newFreq280);
    printf("Rounded\t\t%d\n",newDiscreteFrequency);
    printf("Hex\t\t%x\n",newDiscreteFrequency);
  }
  return 0;
}

int writeNewFrequency(int* newFreq) {

    /* Normalise hz to rads/sample first */
    newFreqNorm = (2 * pi * *newFreq)/fs;

    /* Convert to 28.0 for tone gen block */
    newFreq280 = newFreqNorm * pow(2,23);

    /* Round to integer */
    newDiscreteFrequency = (int)(ceil(newFreq280));

    /* Push out new freq over I2C here in production code. Return 1 if error. */ 

    return 0;
}