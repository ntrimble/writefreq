WriteFreq
---
Small cl routine to convert frequency in Hz into normalized rads/sec and then convert to a 28.0 32-bit hex representation for sending out over I2C to a DSP.

Useful for micros. Written as part of a much bigger codebase, so broken out for testing.
